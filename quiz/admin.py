from django.contrib import admin


from .models.Quiz import Quiz
from .models.Question import Question
from .models.Option import Option
from .models.Session import Session, SessionParticipation, SessionParticipationMetadata

admin.site.register(Quiz)
admin.site.register(Question)
admin.site.register(Option)
admin.site.register(Session)
admin.site.register(SessionParticipation)
admin.site.register(SessionParticipationMetadata)

# Register your models here.

const addNewQuestion = document.querySelector('#add_new-question');
const questionBlockTemplate = document.querySelector('.quiz-question-template');
const questionBlock = document.importNode(questionBlockTemplate.content, true);
const questionContainer = document.querySelector('.container_quiz_create_question_block');
questionContainer.appendChild(questionBlock)

attachListenerToAddAnswerButton = (questionBlock, button) => {
    button.addEventListener("click", function(){
    console.log("buttoncliiiiiiiiiiiick");
    var answers = document.querySelectorAll(".answer-div");
    var newAnswer = document.createElement("div");
    newAnswer.classList.add("answer-div");
    newAnswer.innerHTML = `
      <button class="del-ans" id="del-ans">X</button>
      <input type="text" class="answer-input" id="answer-input_${answers.length+1}" name="option_text" placeholder="Answer ${answers.length+1}">
      <input type="radio" class="answer-right-checkbox" id="answer-checkbox_${answers.length+1}" name="option_is_correct">
    `;
    const answerContainer = questionBlock.querySelector(".answer_container")
    answerContainer.appendChild(newAnswer);
})}

addNewQuestion.addEventListener('click', () => {
  const questionBlock = document.importNode(questionBlockTemplate.content, true);
  const addAnswerButton = questionBlock.querySelector(".add-ans");
  attachListenerToAddAnswerButton(questionBlock, addAnswerButton);
  questionContainer.appendChild(questionBlock)
});

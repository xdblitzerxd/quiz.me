const addNewQuestionBtn = document.querySelector(".add-new-question");

addNewQuestionBtn.addEventListener("click", function() {
  const questionBackground = document.createElement("rect");
  questionBackground.classList.add("quiz-question-background");

  const questionFrame = document.createElement("div");
  questionFrame.classList.add("quiz-question_frame");
  questionBackground.appendChild(questionFrame);

  const questionTitleInput = document.createElement("input");
  questionTitleInput.classList.add("question_title");
  questionTitleInput.setAttribute("type", "text");
  questionTitleInput.setAttribute("name", "question_text");
  questionTitleInput.setAttribute("placeholder", "Текст вопроса");
  questionFrame.appendChild(questionTitleInput);

  const answerDiv = document.createElement("div");
  answerDiv.classList.add("answer-div");
  questionFrame.appendChild(answerDiv);

  const answerInput = document.createElement("input");
  answerInput.classList.add("answer-input");
  answerInput.setAttribute("type", "text");
  answerInput.setAttribute("id", "answer-input_1");
  answerInput.setAttribute("name", "option_text");
  answerInput.setAttribute("placeholder", "1 ответ");
  answerDiv.appendChild(answerInput);

  const answerCheckbox = document.createElement("input");
  answerCheckbox.classList.add("answer-right-checkbox");
  answerCheckbox.setAttribute("type", "radio");
  answerCheckbox.setAttribute("id", "answer-checkbox_1");
  answerCheckbox.setAttribute("name", "option_is_correct");
  answerDiv.appendChild(answerCheckbox);

  const delAnsBtn = document.createElement("button");
  delAnsBtn.classList.add("del-ans");
  delAnsBtn.setAttribute("id", "del-ans");
  delAnsBtn.textContent = "X";
  answerDiv.appendChild(delAnsBtn);

  const addAnsBtn = document.createElement("button");
  addAnsBtn.classList.add("add-ans");
  addAnsBtn.setAttribute("id", "add_ans");
  addAnsBtn.textContent = "Добавить ответ";
  questionFrame.appendChild(addAnsBtn);

  const addAnswerDiv = document.createElement("div");
  addAnswerDiv.setAttribute("id", "add-answer-div");
  questionFrame.appendChild(addAnswerDiv);

  const quizQuestionsBlock = document.querySelector(".container_quiz_create_question_block");
  quizQuestionsBlock.appendChild(questionBackground);
});

const pathName = window.location.pathname
const url = 'ws:' + '127.0.0.1:8000' + pathName
const socket = new WebSocket(url)

const questionTitle = document.querySelector('.question_quiz')
const quizTitle = document.querySelector('.text_h1_quiz')
const submitButton = document.querySelector('.button_quiz')
const radioGroupTemplate = document.querySelector('.radio_group_template')
const sessionFinishedTemplate = document.querySelector('.session_finished_group_template')
const optionGroupDiv = document.querySelector('.div_radio_buttons')
const correctAnswersCountLabel =  document.querySelector('.count_of_questions_left')


let optionsData = {}

socket.onopen = () => {
    console.log('Connection is established')
}

processNextQuestionMessage = (deserializedData) => {
    optionsData = deserializedData['options']
    questionTitle.textContent = deserializedData['text']
    correctAnswersCountLabel.textContent =  deserializedData['current_question_number'] + '/' + optionsData.length


    optionGroupDiv.replaceChildren()

    optionsData.forEach(optionData => {
        const newRadioGroupDiv = document.importNode(radioGroupTemplate.content, true)
        const newOptionLabel = newRadioGroupDiv.querySelector('.radio_button_text')
        newOptionLabel.textContent = optionData['text']
        optionGroupDiv.appendChild(newRadioGroupDiv)
    })
}

processQuizDataMessage = (deserializedData) => {
    const quizTitleText = deserializedData['title']
    quizTitle.textContent = quizTitleText
}

processSessionFinishedMessage = (deserializedData) => {
    console.log('data:' + deserializedData)
    optionGroupDiv.replaceChildren()
    const sessionFinishedGroup = document.importNode(sessionFinishedTemplate.content, true)

    const correctAnswersCount = deserializedData['correct_answers_count']
    const totalQuestionsCount = optionsData.length 

    const correctAnswersCountLabel = sessionFinishedGroup.querySelector('#correct_answer_count')

    correctAnswersCountLabel.textContent = 'Correct answers: ' + correctAnswersCount + '/' + totalQuestionsCount

    optionGroupDiv.appendChild(sessionFinishedGroup)
}

socket.onmessage = (event) => {

    console.log('data:' + event.data)
    const deserializedData = JSON.parse(event.data)
    const message = deserializedData['message']

    switch  (message) {
        case 'session_finished' :
            processSessionFinishedMessage(deserializedData)
            break
        case 'next_question' :
            processNextQuestionMessage(deserializedData)
            break
        case 'quiz_data' : 
            processQuizDataMessage(deserializedData)
            break 
        default:
            break 
    }
}

submitButton.onclick = () => {
    const checkedRadioButton = document.querySelector('input[name="option_group"]:checked')
    const radioGroup = checkedRadioButton.parentElement
    const label = radioGroup.querySelector('.radio_button_text')
    const optionText = label.textContent

    const chosenOptionData = optionsData.find( (optionData, index, array) => {
        return optionData['text'] == optionText
    })

    const chosenOptionId = chosenOptionData['pk']
    const messageData =  {
        'message' : 'option_choice',
        'chosen_option_id' : chosenOptionId
    }

    const serializedData = JSON.stringify(messageData)

    socket.send(serializedData)
}


const addNewQuestionBtn = document.querySelector(".add-new-question");
const quizQuestionsBlock = document.querySelector(".container_quiz_create_question_block");
const questionTemplate = document.querySelector("#quiz-question-template");

addNewQuestionBtn.addEventListener("click", function() {
  const questionClone = questionTemplate.content.cloneNode(true);
  quizQuestionsBlock.appendChild(questionClone);
});

// import config from "./config"


const submitButton = document.querySelector('.quiz-submit')
const csrfToken = document.getElementById('csrf-token').getAttribute('data-token')


submitButton.addEventListener("click", () => {
    create_quiz()
})


function create_quiz(){
    const questionGroups = document.querySelectorAll('.quiz-question-background')
    const quizTitle = document.querySelector('.quiz_title')
    const quizDescription = document.querySelector('.quiz_description')
    // const quizTimeLimited = document.querySelector('.quiz_time_limited')
    const quizIsPrivate = document.querySelector('.quiz_is_private')

    const requestData = {
        header : 'create_quiz',
        title : quizTitle.value,
        description : quizDescription.value,
        // time_limited : quizTimeLimited.checked,
        time_limited : true,
        is_private : quizIsPrivate.checked,
        questions : []
    }

    questionGroups.forEach((questionGroup) => {

        const optionGroups = questionGroup.querySelectorAll('.answer-div.input')
        const questionText = questionGroup.querySelector('.question')
        // const questionType = questionGroup.querySelector('.question_type')
        // const questionTimeLimit = questionGroup.querySelector('.question_time_limit')

        const questionData = {
            text : questionText.value,
            type : 'single_choice',
            time_limit : '20',
            // type : questionType.textContent,
            // time_limit : parseInt(questionTimeLimit.textContent),
            options : []
        }

        optionGroups.forEach((optionGroup) => { 

            const optionText = optionGroup.querySelector('.answer-input')
            // const optionFieldType = optionGroup.querySelector('.option_field_type')
            // const optionIsCorrect = optionGroup.querySelector('.option_is_correct')

            const optionData = {
                text : optionText.value,
                field_type : 'radio_button',
                is_correct : true 
                // field_type : optionFieldType.textContent,
                // is_correct : optionIsCorrect.checked
            }

            questionData.options.push(optionData)
        })

        requestData.questions.push(questionData)
    })

    request_body = JSON.stringify(requestData)

    console.log(request_body)


    fetch('', {
        method : 'POST',
        headers : {
            'Content-Type' : 'application/json',
            'X-CSRFToken' : csrfToken
        },
        body : request_body
    }).then (response => {
        console.log('response' , response)
        if (response.redirected) {
            window.location.href = response.url;
          }
    })
}



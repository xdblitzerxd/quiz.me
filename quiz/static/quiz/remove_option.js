document.addEventListener("DOMContentLoaded", () => {
  const container = document.querySelector(".container_quiz_create_question_block");
  container.addEventListener("click", (event) => {
    const deleteAnswerBtn = event.target.closest(".del-ans");
    if (deleteAnswerBtn) {
      console.log("Button clicked");
      const delAnsButtons = container.querySelectorAll('.del-ans');
      if (delAnsButtons.length !== 1) {
        const answerDiv = deleteAnswerBtn.closest(".answer-div");
        answerDiv.remove();
        const rect = document.querySelector(".quiz-question-background");
        rect.style.height = `${rect.offsetHeight - 60}px`;
      }
    }
  });
});

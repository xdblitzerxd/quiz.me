from django.forms import ModelForm
from quiz.models.Option import Option
from typing import Dict, Any
import logging


class OptionForm(ModelForm):
    """
    Form that takes following option model parameters and validates them

    text 
    field_type
    is_correct
    """

    class Meta:
        model = Option
        fields = ['text', 'field_type', 'is_correct']

    def clean(self) -> Dict[str, Any]:
        cleaned_data = super().clean()

        if self.errors:
            logging.error(f"{self.errors}")

        return cleaned_data

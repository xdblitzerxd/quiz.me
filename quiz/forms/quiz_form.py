from django.forms import ModelForm
from quiz.models.Quiz import Quiz
from typing import Dict, Any
import logging


class QuizForm(ModelForm):
    """
    Form that takes following quiz model parameters and validates them

    title 
    description 
    is_private 
    time_limited 
    """

    class Meta:
        model = Quiz
        fields = ['title', 'description', 'is_private', 'time_limited']

    def clean(self) -> Dict[str, Any]:
        cleaned_data = super().clean()

        if self.errors:
            logging.error(f"{self.errors}")

        return cleaned_data

from django.forms import ModelForm
from quiz.models.Question import Question
from typing import Dict, Any
import logging


class QuestionForm(ModelForm):
    """
    Form that takes following question model parameters and validates them

    text 
    type 
    time_limit 
    """
    class Meta:
        model = Question
        fields = ['text', 'type', 'time_limit']

    def clean(self) -> Dict[str, Any]:
        cleaned_data = super().clean()

        if self.errors:
            logging.error(f"{self.errors}")

        return cleaned_data

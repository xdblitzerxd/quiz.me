from django.db import models
from quiz.managers.QuizManager import QuizManager


class Quiz(models.Model):
    title = models.CharField(max_length=20)
    description = models.CharField(max_length=300)
    is_private = models.BooleanField(default=False)
    subscribers = models.ManyToManyField(through='QuizToUser', to='auth.User')
    time_limited = models.BooleanField()
    manager = QuizManager()


class QuizToUser(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE)
    is_author = models.BooleanField(default=False)
    sessions_participated = models.IntegerField(default=0)
    sessions_created = models.IntegerField(default=0)
    liked = models.BooleanField(default=False)

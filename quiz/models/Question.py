from django.db import models

from quiz.managers.QuestionManager import QuestionManager


class Question(models.Model):

    manager = QuestionManager()

    text = models.CharField(max_length=80)

    QUESTION_TYPES = (
        ('single_choice', 'Single choice'),
        ('multiple_choice', 'Multiple choice'),
        ('binary_choice', 'Binary choice')
    )

    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE)
    type = models.CharField(
        max_length=20, choices=QUESTION_TYPES)  # to think about
    time_limit = models.IntegerField()
    # think about how to connect with session

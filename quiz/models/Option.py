from django.db import models
from quiz.managers.OptionManager import OptionManager


class OptionMetadata(models.Model):
    description = models.CharField(max_length=50, null=True)
    image = models.CharField(max_length=50, null=True)
    option = models.OneToOneField(
        'Option',
        on_delete=models.CASCADE,
        primary_key=True
    )


class Option(models.Model):
    FIELD_TYPES = (
        ('input', 'Input'),
        ('radio_button', 'Radio button'),
        ('checkbox', 'Checkbox')
    )
    text = models.CharField(max_length=40)
    field_type = models.CharField(max_length=20, choices=FIELD_TYPES)
    is_correct = models.BooleanField()
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    manager = OptionManager()

from django.db import models 
from ..managers.session_managers import SessionManager, SessionParticipationManager, SessionParticipationMetadataManager


class Session(models.Model):
    quiz = models.ForeignKey("Quiz", on_delete= models.CASCADE, null = True)
    users = models.ManyToManyField(through= "SessionParticipation", to = "auth.User")
    is_finsihed = models.BooleanField(default= False)
    manager = SessionManager()

class SessionParticipationMetadata(models.Model):
    manager = SessionParticipationMetadataManager()
    session_participation = models.ForeignKey("SessionParticipation", on_delete= models.CASCADE)
    questions = models.ManyToManyField(through= "SessionParticipationMetadataToQuestion", to = "Question")

class SessionParticipationMetadataToQuestion(models.Model):
    session_participation_metadata = models.ForeignKey("SessionParticipationMetadata", on_delete= models.CASCADE)
    question = models.ForeignKey("Question", on_delete= models.CASCADE)
    is_answered = models.BooleanField(default= False)
   

class SessionParticipationToOption(models.Model):
    session = models.ForeignKey("SessionParticipation", on_delete= models.CASCADE)
    option = models.ForeignKey("Option", on_delete= models.CASCADE)
    

class SessionParticipation(models.Model):
    is_author = models.BooleanField()
    manager = SessionParticipationManager()
    user = models.ForeignKey("auth.User", on_delete= models.CASCADE)
    session = models.ForeignKey("Session", on_delete= models.CASCADE)
    is_finsihed = models.BooleanField(default= False)
    options = models.ManyToManyField(through= "SessionParticipationToOption", to = "Option")


from django.urls import re_path

from .consumers.session_consumer import SessionConsumer

websocket_urlpatterns = [
    re_path(r"quiz/(?P<quiz_id>\d+)/session/(?P<session_id>\d+)/$", SessionConsumer.as_asgi())
]
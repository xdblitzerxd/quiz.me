from django.test import TestCase
from django.contrib.auth.models import User
from .models.Quiz import Quiz
from .models.Question import Question
import logging

class CreateQuizTestCase(TestCase):

    def test_quiz_create_request_1(self):
        self.__create_initial_dataset()
    
    def test_quiz_edit_request_1(self):
        self.__create_initial_dataset()

        created_quiz = Quiz.manager.get_all_public_quizes()[0]
        created_question = Question.manager.get_all_questions_by_quiz(created_quiz)[0]

        edit_request_data = {
            "header" : "edit_questions_in_quiz",
            "quiz_id" : created_quiz.pk,
            "questions" : [{
                "id" : created_question.pk,
                "edited_fields" : {
                    "text" : "Edited test question 1_1",
                    "type" : "multiple_choice",
                    "time_limit" : 15,
                    }
                ,
                "options" : [{
                    "id" : 1,
                    "edited_fields" : {
                        "text" : "Edited test option 1",
                        "field_type" : "checkbox",
                        "is_correct" : False
                    }
                }]
                }
            ]   
        }

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = edit_request_data, content_type= "application/json")
    
    def test_add_options_to_questions_in_quiz(self):

        self.__create_initial_dataset()

        created_quiz = Quiz.manager.get_all_public_quizes()[0]
        created_question = Question.manager.get_all_questions_by_quiz(created_quiz)[0]

        request_data = {
            "header" : "add_options_to_questions_in_quiz",
            "quiz_id" : created_quiz.pk,
            "questions" : [
                {
                "id" : created_question.pk,
                "options" : [
                        {
                        "text" : "added option text 1",
                        "field_type" : "radio_button",
                        "is_correct" : True
                        }
                    ]
                }
            ]   
		}

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")
    
    def test_add_questions_to_quiz(self):
        self.__create_initial_dataset()

        created_quiz = Quiz.manager.get_all_public_quizes()[0]

        request_data = {
            
            "header" : "add_questions_to_quiz",
            "quiz_id" : created_quiz.pk,
            "questions" : [{
                "text" : "added question 1",
				"type" : "multiple_choice",
				"time_limit" : 17,
				"options" : [{
						"text" : "added option 2",
						"field_type" : "checkbox",
						"is_correct" : False
                        }]
            }
            ] 
		}

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")
    
    def test_edit_quiz_privacy(self):
        self.__create_initial_dataset()
        created_quiz = Quiz.manager.get_all_public_quizes()[0]

        request_data = {      
            "header" : "edit_quiz_privacy",
            "id" : created_quiz.pk,
            "is_private" : True
        }

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")
    
    def test_edit_quiz_description(self):
        self.__create_initial_dataset()
        created_quiz = Quiz.manager.get_all_public_quizes()[0]

        request_data = {      
            "header" : "edit_quiz_description",
            "id" : created_quiz.pk,
            "description" : "new_description"
        }

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")
    
    def test_edit_quiz_title(self):
        self.__create_initial_dataset()
        created_quiz = Quiz.manager.get_all_public_quizes()[0]

        request_data = {      
            "header" : "edit_quiz_title",
            "id" : created_quiz.pk,
            "title" : "new_title"
        }

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")
    
    def test_delete_questions_from_quiz(self):
        self.__create_initial_dataset()
        created_quiz = Quiz.manager.get_all_public_quizes()[0]
        created_question = Question.manager.get_all_questions_by_quiz(created_quiz)[0]
        
        request_data = {
            "header" : "delete_questions_from_quiz",
            "quiz_id" : created_quiz.pk, 
            "questions" : [
                {
                    "question_id" :created_question.pk 
                }
            ]
        }

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")
    
    def test_delete_options_from_questions_in_quiz(self):
        self.__create_initial_dataset()
        created_quiz = Quiz.manager.get_all_public_quizes()[0]
        
        request_data = {
            "header" : "delete_options_from_questions_in_quiz",
            "options" : [
                {
                    "id" : 1
                }
            ]
        }

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")
    
    def test_delete_quiz(self):
        self.__create_initial_dataset()
        created_quiz = Quiz.manager.get_all_public_quizes()[0]

        request_data = {
            "header" : "delete_quiz",
            "quiz_id" : created_quiz.pk
        }

        self.client.post(f"/quiz/{created_quiz.pk}/settings/", data = request_data, content_type= "application/json")


    def __create_initial_dataset(self):
        user = User.objects.create_user(
        username= "test_user_1",
        password= "test_password_1",
        email = "testemail@gmail.com",
        id = 1
        )
        self.client.force_login(user = user)
        create_request_data = {
            "header" : "create_quiz",
            "title" : "Test quiz 1",
            "description" : "Test description 1",
            "time_limited" : True,
            "is_private" : False,
            "questions" : [
                {   "text" : "Test question 1_1",
                    "type" : "single_choice",
                    "time_limit" : 30,
                    "options" : [
                        {
                        "text" : "test option 1",
                        "field_type" : "radio_button",
                        "is_correct" : True
                        }
                    ]
                }
            ]
        }

        self.client.post("/quiz/create/", data = create_request_data, content_type = "application/json")




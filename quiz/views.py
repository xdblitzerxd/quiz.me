from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from quiz.services.quiz_get_service import QuizGetService
from quiz.services.quiz_post_create_service import QuizPostCreateService
from quiz.services.quiz_post_delete_service import QuizPostDeleteService
from quiz.services.quiz_post_edit_service import QuizPostEditService
from django.core.exceptions import BadRequest
from django.views.generic import View
from typing import Any, Dict
import json
import logging 


class QuizBaseView(View):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._error_messages = {
            "header_is_none" : "request header is none",
            "header_is_invalid" : "request header is invalid"
        }
        self._get_service = QuizGetService()

        self._post_methods_by_header = {}
    

    def _deserialize_request_body(self,request : HttpRequest) -> Dict:
        decoded_body = request.body.decode("utf-8")
        return json.loads(decoded_body)

    def _validate_header(self, deserialized_body : Dict):
        if not "header" in deserialized_body :
            raise BadRequest(self._error_messages["header_is_none"])

        if not deserialized_body["header"] in self._post_methods_by_header : 
            logging.debug(f"header : {deserialized_body['header']}" )
            raise BadRequest(self._error_messages["header_is_invalid"])

    def _dispatch_post_request(self, deserialized_body : Dict):
        header = deserialized_body["header"]
        self._post_methods_by_header[header](request_data = deserialized_body)    



@method_decorator(login_required(login_url='/', redirect_field_name=None), name = "dispatch")
class QuizCreatePageView(QuizBaseView):

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.__template_name = "quiz_create.html"
        self.__post_create_service = QuizPostCreateService()
        self._post_methods_by_header = {
            "create_quiz" : self.__post_create_service.process_create_quiz_request
        } 
    
    def _dispatch_post_request(self, deserialized_body: Dict, request : HttpRequest):
        header = deserialized_body["header"]
        self._post_methods_by_header[header](
            request_data = deserialized_body, request= request) 
        
    
    def post(self, request : HttpRequest) -> HttpResponse :
        try :
            logging.debug(f"body : {request.body}")
            deserialized_body = self._deserialize_request_body(request)
            self._validate_header(deserialized_body)
            self._dispatch_post_request(deserialized_body, request= request)

        except Exception as exception : 
            logging.error(exception)
        
        finally:
            return redirect("quiz:main_page")   

    def get(self, request : HttpRequest) -> HttpResponse :
        return render(request, template_name= self.__template_name, context = {})


class QuizMainPageView(QuizBaseView):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        
        self.__template_name = "quiz_main.html"
    
    def get(self, request : HttpRequest) -> HttpResponse :
        context = self._get_service.get_quizes_page_context(request_data= None) 
        return render(request, template_name = self.__template_name, context = context)

class QuizPageView(QuizBaseView):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        
        self.__template_name = "quiz.html"
    
    def get(self, request : HttpRequest, id : int) -> HttpResponse :
        context = self._get_service.get_quiz_page_context(request_data = None, quiz_id= id)
        return render(request, template_name = self.__template_name, context = context)

class QuizSettingsPageView(QuizBaseView):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        
        self.__template_name = "quiz_settings.html"
        self.__post_edit_service = QuizPostEditService()
        self.__post_delete_service = QuizPostDeleteService()
        self.__post_create_service = QuizPostCreateService()

        self._post_methods_by_header = {
            "edit_questions_in_quiz" : self.__post_edit_service.process_edit_questions_in_quiz_request,
            "edit_quiz_privacy" : self.__post_edit_service.process_edit_quiz_privacy_request,
            "edit_quiz_description" : self.__post_edit_service.process_edit_quiz_description_request,
            "edit_quiz_title" : self.__post_edit_service.process_edit_quiz_title_request,
            "delete_options_from_questions_in_quiz" : self.__post_delete_service.process_delete_options_from_questions_in_quiz_request,
            "delete_questions_from_quiz" : self.__post_delete_service.process_delete_questions_from_quiz_request,
            "delete_quiz" : self.__post_delete_service.process_delete_quiz_request,
            "add_options_to_questions_in_quiz" : self.__post_create_service.process_add_options_to_questions_in_quiz_request,
            "add_questions_to_quiz" : self.__post_create_service.process_add_questions_to_quiz_request,
        }

    def post(self, request : HttpRequest, id : int) -> HttpResponse :
        try :
            deserialized_body = self._deserialize_request_body(request)
            self._validate_header(deserialized_body)
            self._dispatch_post_request(deserialized_body)
        except Exception as exception:
            logging.log(exception)
        finally:
            return self.__render_response(request, id)
    
    def __render_response(self, request : HttpRequest, id : int) -> Dict :
        context = self._get_service.get_quiz_settings_page_context(request_data= None, quiz_id= id)
        return render(request, template_name= self.__template_name, context = context)

    
    def get(self, request : HttpRequest, id : int) -> HttpResponse :
        return self.__render_response(request, id)


@method_decorator(login_required(login_url='/', redirect_field_name=None), name = "dispatch")
class QuizSessionPageView(QuizBaseView):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        
        self.__template_name = "quiz_session.html"

    def get(self, request : HttpRequest, quiz_id : int, session_id : int) -> HttpResponse :
        return render(request, template_name = self.__template_name, context = {})
    

    


# def main_page(request: HttpRequest):
#     context = {}

#     if request.method == "GET":
#         service = QuizGetService()
#         context = service.get_quizes_page_context(request)

#     return render(request, template_name='quiz_main.html', context=context)


# def quiz_page(request: HttpRequest, id: int):
#     context = {}

#     if request.method == "GET":
#         service = QuizGetService()
#         context = service.get_quiz_page_context(request, quiz_id=id)
        
            

#     return render(request, template_name='quiz.html', context=context)


# def quiz_create_page(request):
#     return render(request, template_name='quiz_create.html')


# def quiz_settings_page(request: HttpRequest, id: int):
#     context = {}

#     if request.method == "GET":
#         service = QuizGetService()
#         context = service.get_quiz_settings_page_context(request, quiz_id=id)

#     return render(request, template_name='quiz_settings.html', context=context)


# def quiz_session_page(request):
#     return render(request, template_name='quiz_session.html')


class InvalidDataException(Exception):
    pass 

class NoModelFoundByIdException(Exception):
    pass 
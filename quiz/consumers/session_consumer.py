import json
from channels.generic.websocket import WebsocketConsumer
from ..models.Session import Session, SessionParticipation, SessionParticipationMetadata
from ..models.Quiz import Quiz 
from ..models.Option import Option
from ..models.Question import Question
from django.core import serializers
from asgiref.sync import async_to_sync
from typing import Dict
import logging 

class SessionConsumer(WebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.session_manager = Session.manager
        self.option_manager = Option.manager
        self.session_participation_manager = SessionParticipation.manager
        self.quiz_manager = Quiz.manager
        self.question_manager = Question.manager
        self.session_participation_metadata_manager =  SessionParticipationMetadata.manager
        self.session_id = None
        self.quiz_id = None 
        self.session_participation_id = None 
        self.session_group_name = None

    def connect(self):
        self.quiz_id = self.scope["url_route"]["kwargs"]["quiz_id"]
        self.__create_session_models(quiz_id= self.quiz_id)
        self.session_group_name = "session_%s" % self.session_id

        async_to_sync(self.channel_layer.group_add)(
            self.session_group_name, self.channel_name
        )

        self.accept()

        self.__send_quiz_data_message_to_client()
        self.__send_next_question_data_to_client()

     

    def disconnect(self, close_code):
        # Quit session
        async_to_sync(self.channel_layer.group_discard)(
            self.session_group_name, self.channel_name
        )
    
    # Receive message from WebSocket
    def receive(self, text_data):
        logging.debug("received message from client!")
        text_data_json = json.loads(text_data)
        self.__process_chosen_option_message(text_data= text_data_json)

    
    def __process_chosen_option_message(self, text_data : Dict):

        chosen_option_id = text_data["chosen_option_id"]
        chosen_option = self.option_manager.get_option_by_id(id = chosen_option_id)
        session_participation_metadata = self.__get_current_session_participation_metadata()
        session = self.session_manager.get_session_by_id(id = self.session_id)
        session_participation = self.session_participation_manager.get_session_participation_by_id(
            id = self.session_participation_id)

        logging.debug(f"""
        Retrieved models from managers : 
        - session [{session}]
        - session_participation_metadata [{session_participation_metadata}]
        - session_participation [{session_participation}]
        """
        )

        self.option_manager.add_new_option_to_session_participation(
            option = chosen_option, 
            session_participation= session_participation
        ) 

        self.session_participation_metadata_manager.set_last_question_answered_in_session_participation_metadata(session_participation_metadata=
        session_participation_metadata)

        all_questions_answered = self.session_participation_metadata_manager.all_questions_answered_in_session_participation_metadata(
            session_participation_metadata = session_participation_metadata
        )

        if not all_questions_answered:
            logging.debug("questions left")
            self.__send_next_question_data_to_client()
        else:
            self.session_participation_manager.set_session_participation_finished(
                session_participation = session_participation)
            
            self.__send_session_participation_finished_message_to_client()
            
            # self.session_manager.set_session_finished(session)
            # self.__send_session_finished_event_to_group()
    

    def __send_quiz_data_message_to_client(self):
        quiz = self.quiz_manager.get_quiz_by_id(self.quiz_id)
        
        message_data = {
            'message' : 'quiz_data',
            'title' : quiz.title,
            'time_limited' : quiz.time_limited,
            'description' : quiz.description, 
            'pk' : quiz.pk,
            'is_private' : quiz.is_private
        }

        dumped_data = json.dumps(message_data)

        self.send(
            text_data= dumped_data
        )

        logging.debug("sent quiz_data message to client")

    def __send_session_participation_finished_message_to_client(self):

        session_participation = self.session_participation_manager.get_session_participation_by_id(self.session_participation_id)
        correct_options  = self.session_participation_manager.get_correct_options_for_session_participation(session_participation= session_participation)
        message_data =  {
            'message' : 'session_finished',
            'correct_answers_count' : correct_options.count()
        }
        dumped_data = json.dumps(message_data)

        self.send(
            text_data= dumped_data
        )

        logging.debug("sent session participation finished message to client")

    def __send_next_question_data_to_client(self):
        session_participation_metadata = self.__get_current_session_participation_metadata()

        next_question = self.session_participation_metadata_manager.get_next_question_to_answer_in_session_participation_metadata(
            session_participation_metadata= session_participation_metadata
        )

        current_question_number = self.session_participation_metadata_manager.get_current_question_number(session_participation_metadata= session_participation_metadata)

        logging.debug(f"next question : {next_question}")

        next_question_options = self.option_manager.get_all_options_by_question(next_question)
        
        options_data = []

        for option in next_question_options :
            options_data.append({
                "pk" : option.pk,
                "text" : option.text,
                "field_type" : option.field_type,
                "is_correct" : option.is_correct
            })
        
        next_question_data = {
            "message" : "next_question",
            "pk" : next_question.pk,
            "text" : next_question.text,
            "type" : next_question.type,
            "time_limit" : next_question.time_limit,
            "options" : options_data,
            "current_question_number" : current_question_number
        }

        logging.debug(f"next question data to the client : {next_question_data}")
        dumped_data =  json.dumps(next_question_data)

        logging.debug(f"dumped data : {dumped_data}")

        self.send(text_data= dumped_data)



    
    def __create_session_models(self, quiz_id : int):
        session = self.session_manager.create_session()
        session_participation = self.session_participation_manager.add_new_session_participation_to_session(
            is_author= True,
            user = self.scope["user"],
            session = session
        )
        session_participation_metadata = self.session_participation_metadata_manager.create_metadata_for_session_participation(
            session_participation= session_participation)

        logging.debug(f"""
        Created models : 
        - session [{session}]
        - session_participation_metadata [{session_participation_metadata}]
        - session_participation [{session_participation}]
        """
        )

        self.session_id = session.pk 
        self.session_metadata_id = session_participation_metadata.pk
        self.session_participation_id = session_participation.pk 

        quiz = self.quiz_manager.get_quiz_by_id(id = quiz_id)
        
        self.quiz_manager.add_quiz_to_session(quiz= quiz, session= session)
        self.__add_quiz_questions_to_metadata(
            quiz = quiz, metadata= session_participation_metadata) 
    
    def __add_quiz_questions_to_metadata(self, quiz : Quiz, metadata:  SessionParticipationMetadata):
        questions = self.question_manager.get_all_questions_by_quiz(quiz= quiz)
        for question in questions:
            self.question_manager.add_question_to_session_participation_metadata(session_participation_metadata= metadata, question= question)
            logging.debug(f"added question to participation metadata : {question}")


    def __get_current_session_participation_metadata(self):
        session_participation = self.session_participation_manager.get_session_participation_by_id(
            self.session_participation_id)
        return self.session_participation_metadata_manager.get_session_participation_metadata_by_session_participation(
            session_participation = session_participation
        )


      # def session_finished_message(self, event): 
    #     session_participation = self.session_participation_manager._session_participation_by_id(id = self.session_participation_id)
    #     correct_options_for_session = self.session_participation_manager.get_correct_options_for_session_participation(
    #         session_participation= session_participation
    #     )

    #     correct_options_for_session_serialized_data  = serializers.serialize(
    #         "json", correct_options_for_session
    #     )

    #     correct_options_for_session_serialized_data["type"] = "session_finished"

    #     self.send(text_data= {
    #         json.dumps(correct_options_for_session_serialized_data)
    #     })
    
    # def __send_session_finished_event_to_group(self):
    #     async_to_sync(self.channel_layer.group_send)(
    #         self.session_group_name, {"type" : "session_finished_message"}
    #     )
        
from django.db import models
import quiz.models.Session as Session
import quiz.models.Option as Option
import quiz.models.Question as Question
from django.contrib.auth.models import User
from typing import Any
import logging


class SessionManager(models.Manager):
    def create_session(self) -> 'Session.Session' :
        try: 
            session = self.model()

            session.save()

            logging.debug(f"session = {session}")
            logging.debug(f"created session with id {session.pk}")

            return session
        except Exception as exception :
            logging.error(exception)
            return None

    def session_exists_by_id(self, id : int) -> bool :
        try :
            _ = self.get_queryset().get(id = id)
            return True 
        except Exception as exception :
            logging.debug("session doesn't exist")
            logging.error(exception)
            return False 

    def get_session_by_id(self, id : int) -> 'Session.Session' :
        try: 
            return self.get_queryset().get(id = id)
        except Exception as exception :
            logging.error(exception)
            return None  

    def set_session_finished(self, session : 'Session.Session') :
        try: 
            session.is_finsihed = True 
            session.save()
            
        except Exception as exception :
            logging.error(exception)


class SessionParticipationMetadataManager(models.Manager):
    def create_metadata_for_session_participation(self, session_participation : 'Session.SessionParticipation') -> 'Session.SessionParticipationMetadata':
        try:
            session_participation_metadata = self.model(
                session_participation = session_participation
            )

            session_participation_metadata.save()

            logging.debug(f"created session participation metadata : {session_participation_metadata}")

            return session_participation_metadata
        except Exception as exception:
            logging.error(exception)
            return None 
    
    def get_session_participation_metadata_by_session_participation(self, session_participation : 'Session.SessionParticipation') -> 'Session.SessionParticipationMetadata' : 
        try: 
            return self.get_queryset().get(session_participation = session_participation)
        except Exception as exception:
            logging.error(exception)
            return None 
    
    def set_last_question_answered_in_session_participation_metadata(self, session_participation_metadata : 'Session.SessionParticipationMetadata'):
        try:
           metadata_to_question = session_participation_metadata.sessionparticipationmetadatatoquestion_set.filter(is_answered = False).first()
           metadata_to_question.is_answered = True
           metadata_to_question.save()

           logging.debug(f"set last question answered in session participation metadata : {session_participation_metadata}")
        except Exception as exception:
            logging.error(exception)

    def get_next_question_to_answer_in_session_participation_metadata(self, session_participation_metadata : 'Session.SessionParticipationMetadata') -> 'Question.Question':
        try:
            metadata_to_question = session_participation_metadata.sessionparticipationmetadatatoquestion_set.filter(is_answered = False).first()
            return metadata_to_question.question
            
        except Exception as exception :
            logging.error(exception)
            return None
    

    def all_questions_answered_in_session_participation_metadata(self, session_participation_metadata : 'Session.SessionParticipationMetadata') -> bool :
        try: 
            metadata_to_question_set = session_participation_metadata.sessionparticipationmetadatatoquestion_set.filter(is_answered = False)
            return metadata_to_question_set.count() == 0
        except Exception as exception :
            logging.error(exception)
            return False
    

    def get_current_question_number(self, session_participation_metadata : 'Session.SessionParticipationMetadata') -> models.QuerySet['Question.Question'] :
        try:
            metadata_to_question_set = session_participation_metadata.sessionparticipationmetadatatoquestion_set.filter(is_answered = True)

            return metadata_to_question_set.count() + 1
            
        except Exception as exception :
            logging.error(exception)


class SessionParticipationManager(models.Manager):
    def add_new_session_participation_to_session(self, user : User, session : 'Session.Session', is_author : bool) -> 'Session.SessionParticipation' :
        try:
            session.users.add(user, through_defaults= {'is_author' : is_author})
            session.save()
            session_participation = self.get_queryset().filter(user = user).last()
            logging.debug(f"session participation : {session_participation}")
            return session_participation
        except Exception as exception:
            logging.error(exception)
            return None 
    def get_session_participation_by_id(self, id : int) -> 'Session.SessionParticipation':
        try:
            return self.get_queryset().get(id = id)
        except Exception as exception:
            logging.error(exception)
    def get_correct_options_for_session_participation(self, session_participation : 'Session.SessionParticipation') -> models.QuerySet['Option.Option'] :
        try:
            correct_options = session_participation.sessionparticipationtooption_set.filter(option__is_correct = True)
            return correct_options
        except Exception as exception:
            logging.error(exception)
    
    def set_session_participation_finished(self, session_participation : 'Session.SessionParticipation') :
        try: 
            session_participation.is_finsihed = True 
            session_participation.save()

            logging.debug(f"set session participation finished : {session_participation}")
            
        except Exception as exception :
            logging.error(exception)



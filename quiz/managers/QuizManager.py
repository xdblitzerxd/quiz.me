from django.db import models
from django.contrib.auth.models import User
import quiz.models.Quiz as Quiz
import quiz.models.Session as Session
from django.db.models import IntegerField
import logging


class QuizManager(models.Manager):

    def get_all_public_quizes(self) -> models.QuerySet['Quiz.Quiz']:
        """ returns all public quizes

        Parameters
        ----------
        None

        Returns
        -------
        all_public_quizes : models.QuerySet['Quiz.Quiz']
        """
        all_public_quizes = self.get_queryset().filter(is_private=False)

        logging.debug(msg=f"returned all public quizes {all_public_quizes}")

        return all_public_quizes

    def get_all_quizes_created_by_user(self, user: User) -> models.QuerySet['Quiz.Quiz']:
        """ returns all quizes created by user

        Parameters
        ----------
        user : User

        Returns
        -------
        all_quizes_created_by_user : models.QuerySet['Quiz.Quiz']
        """
        all_quizes_created_by_user = self.get_queryset().filter(
            subscribers=user, subscribers__is_author=True)

        logging.debug(
            msg=f"returned all quizes created by user {all_quizes_created_by_user}")

        return all_quizes_created_by_user

    def get_all_quizes_liked_by_user(self, user: User) -> models.QuerySet['Quiz.Quiz']:
        """ returns all quizes liked by user

        Parameters
        ----------
        user : User

        Returns
        -------
        all_quizes_liked_by_user : models.QuerySet['Quiz.Quiz']
        """
        all_quizes_liked_by_user = self.get_queryset().filter(
            subscribers=user, subscribers__liked=True)

        logging.debug(
            msg=f"returned all quizes liked by user {all_quizes_liked_by_user}")


    def get_all_public_quizes_by_query(self, query: str) -> models.QuerySet['Quiz.Quiz']:
        """ returns all quizes by query

        Parameters
        ----------
        user : User
        query: str 

        Returns
        -------
        all_quizes_by_query : models.QuerySet['Quiz.Quiz']
        """

        all_public_quizes_by_query = self.get_queryset(
            title__contains=query).filter(is_private=False)

        logging.debug(
            msg=f"returned all quizes by query {all_public_quizes_by_query}")


    def get_quiz_by_id(self, id: IntegerField) -> 'Quiz.Quiz':
        """ returns quiz by id

        Parameters
        ----------
        id : IntegerField 
        Returns
        -------
        quiz : Quiz
        """
        try:
            quiz = self.get_queryset().get(id=id)

            logging.debug(msg=f"returned quiz by id : {quiz}")

            return quiz 

        except Exception as exception:
            logging.error(msg=f"{exception}")

            return None

        

    def create_quiz(self, time_limited : bool,  user: User, title: str, description: str, is_private: bool = False) -> 'Quiz.Quiz':
        """ creates quiz and returns it

        Parameters
        ----------
        user : User
        title : str
        description : str 
        questions : tuple['Question.Question']
        is_private : bool

        Returns
        -------
        quiz : Quiz
        """
        try:
            quiz = self.model(
                time_limited = time_limited,
                title=title,
                description=description,
                is_private=is_private
            )

            quiz.save()

            quiz.subscribers.add(user, through_defaults = {"is_author" : True})

            

            logging.debug(msg=f"Created quiz {quiz}")

            return quiz

        except Exception as exception:
            logging.error(msg=f"{exception}")

            return None

    def delete_quiz(self, quiz: 'Quiz.Quiz'):
        """ Deletes quiz 
        Parameters
        ----------
        quiz : Quiz.Quiz 
        Returns
        -------
        """
        try:
            quiz.delete()
            logging.debug(msg="Deleted quiz")

        except Exception as exception:
            logging.error(msg=f"{exception}")
    
    def edit_quiz_description_field(self, description : str, quiz : 'Quiz.Quiz'):
        """ Edits quiz description field 
        Parameters
        ----------
        description : str
        quiz : Quiz 
        Returns
        -------
        """
        try:
            quiz.description = description
            quiz.save()

            logging.debug("edited quiz description field")
        except Exception as exception:
            logging.error(exception)
    
    def edit_quiz_title_field(self, title : str, quiz : 'Quiz.Quiz'):
        """ Edits quiz title field 
        Parameters
        ----------
        title : str
        quiz : Quiz 
        Returns
        -------
        """
        try:
            quiz.title = title
            quiz.save()

            logging.debug("edited quiz title field")
        except Exception as exception:
            logging.error(exception)
    
    def edit_quiz_privacy_field(self, is_private : bool, quiz : 'Quiz.Quiz'):
        """ Edits quiz description field 
        Parameters
        ----------
        is_private : bool
        quiz : Quiz 
        Returns
        -------
        """
        try:
            quiz.is_private = is_private
            quiz.save()

            logging.debug("edited quiz is_private field")
        except Exception as exception:
            logging.error(exception)
    
    def add_quiz_to_session(self, quiz : 'Quiz.Quiz', session : 'Session.Session'):
        try:
            session.quiz = quiz
            session.save()
            
        except Exception as exception:
            logging.error(exception)
            
    
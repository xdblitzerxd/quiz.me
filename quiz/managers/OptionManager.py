from django.db import models
import quiz.models.Option as Option
import quiz.models.Question as Question
from typing import Any
import quiz.models.Session as Session
import logging

class OptionManager(models.Manager):
    def delete_option(self, option: 'Option.Option'):
        """ deletes option

        Parameters
        ----------
        option : Option

        Returns
        -------
        """
        try:
            option.delete()

            logging.debug("deleted option")

        except Exception as exception:
            logging.error(exception)
    

    def get_option_by_id(self, id : int) ->'Option.Option':
        """ returns option by id

        Parameters
        ----------
        option : Option

        Returns
        -------
        """
        try:
            option = self.get_queryset().get(id = id)
            logging.debug(f"return option by id: {id}")
            
            return option
     
        except Exception as exception:
            logging.error(exception)
            return None
    
    def edit_fields_for_option(self, option : 'Option.Option', **kwargs):
        """ edits provided fields for option in kwargs dictionary

        Parameters
        ----------
        option : Option 
        args : {
            text : str 
            field_type : str 
            is_correct : bool 
        }

        Returns
        -------
        """
        text = kwargs.get("title")
        field_type = kwargs.get("field_type")
        is_correct = kwargs.get("is_correct")

        if self.__parameter_is__not_null(text):
            self.__edit_option_text_field(text, option)
        
        if self.__parameter_is__not_null(field_type):
            self.__edit_option_field_type_field(field_type, option)
        
        if self.__parameter_is__not_null(is_correct):
            self.__edit_option_is_correct_field(is_correct, option)

    def __edit_option_text_field(self, text : str, option : 'Option.Option') :
        try:
            option.text = text 
            option.save()

            logging.debug("edited option text field")
        
        except Exception as exception : 
            logging.error(exception)
    
    def __edit_option_field_type_field(self, field_type : str, option : 'Option.Option') :
        try:
            option.field_type = field_type 
            option.save()

            logging.debug("edited option field_type field")
        
        except Exception as exception : 
            logging.error(exception)
    
    def __edit_option_is_correct_field(self, is_correct : bool, option : 'Option.Option') :
        try:
            option.is_correct = is_correct
            option.save()

            logging.debug("edited option is_correct field")
        
        except Exception as exception : 
            logging.error(exception)
    
    def __parameter_is__not_null(self, value : Any) -> bool :
        """ checks if the value is not null
        Parameters
        ----------
        value : Any
        Returns
        -------
        is_null : bool 
        """
        return value is not None 
    

    def add_new_option_to_session_participation(self, option : 'Option.Option', session_participation : 'Session.SessionParticipation'):
        try:
            session_participation.options.add(option)
            session_participation.save()

            logging.debug(f"Added new option to user session participation : {option} ")
        except Exception as exception:
            logging.error(exception)
    
    def get_all_options_by_question(self, question : 'Question.Question')  -> models.QuerySet['Option.Option'] :
        return self.get_queryset().filter(question = question)
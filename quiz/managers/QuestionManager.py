from django.db import models
from django.contrib.auth.models import User
from quiz.models.Quiz import Quiz
from quiz.models.Option import Option
import quiz.models.Question as Question
from django.db.models import IntegerField
import quiz.models.Session as Session 
from typing import Any
import logging


class QuestionManager(models.Manager):

    def create_question_for_quiz(self, quiz: Quiz, text: str, type: str, time_limit: str) -> 'Question.Question':
        """ creates a question 

        Parameters
        ----------
        text : string 
        quiz: Quiz
        type: string 
        time_limit : string

        Returns
        -------
        question : Question
        """
        try:
            question = self.model(
                text=text,
                type=type,
                time_limit=time_limit
            )

            question.quiz = quiz

            question.save()
         

            logging.debug(msg=f"created question : {question}")

            return question

        except Exception as exception:
            logging.error(msg=f"{exception}")

            return None

    def get_all_questions_by_quiz(self, quiz: Quiz) -> models.QuerySet['Question.Question']:
        """ returns all question by quiz 

        Parameters
        ----------
        quiz : Quiz 

        Returns
        -------
        all_question_by_quiz : QuerySet<Question> 
        """
        all_questions_by_quiz = self.get_queryset().filter(quiz=quiz)

        logging.debug(
            msg=f"returned all questions by quiz : {all_questions_by_quiz}")

        return all_questions_by_quiz

    def delete_question(self, question_id: IntegerField):
        """ deletes a question 

        Parameters
        ----------
        question_id : IntegerField

        Returns
        -------
        """
        try:
            question = self.get_queryset().get(id=question_id)
            question.delete()

            logging.debug(msg=f"deleted question with id : {question_id}")


        except Exception as exception:
            logging.error(msg=f"{exception}")

    def create_option_for_question(self, question: 'Question.Question', is_correct: bool, field_type: str, text: str):
        """ create option for question

        Parameters
        ----------
        question : Question
        is_correct : bool 
        field_type : string 
        text : string  

        Returns
        -------
        """
        try:
            option = Option(
                field_type=field_type,
                text=text,
                is_correct=is_correct
            )

            option.question = question

            option.save()

            logging.debug(msg=f"Created option for question {option}")

        except Exception as exception:
            logging.error(msg=f"{exception}")


    def get_question_by_id(self, id: IntegerField) -> 'Question.Question':
        """ returns question by its id 

        Parameters
        ----------
        id : IntegerField 

        Returns
        -------
        question : Question 
        """
        try:
            question = self.get_queryset().get(id=id)

            logging.debug(msg=f"returned question by id : {question}")

            return question

        except Exception as exception:
            logging.error(msg=f"{exception}")

            return None

    def edit_fields_for_question(self, question: 'Question.Question', **kwargs):
        """ edits provided fields for question in kwargs dictionary

        Parameters
        ----------
        question: Question 
        args : {
            text : str 
            type : str
            time_limit : int
        }

        Returns
        -------
        """
        text = kwargs.get("text")
        type = kwargs.get("type")
        time_limit = kwargs.get("time_limit")

        if self.__parameter_is__not_null(text):
            self.__edit_question_text_field(text, question)

        if self.__parameter_is__not_null(type):
            self.__edit_question_type_field(type, question)

        if self.__parameter_is__not_null(time_limit):
            self.__edit_question_time_limit_field(time_limit, question)

    def __edit_question_text_field(self, text : str, question : 'Question.Question') :
        try:
            question.text = text 
            question.save()

            logging.debug("edited question text field")

        except Exception as exception : 
            logging.error(exception)

    def __edit_question_type_field(self, type : str, question : 'Question.Question') :
        try:
            question.type = type
            question.save()

            logging.debug("edited question type field")

        except Exception as exception : 
            logging.error(exception)

    def __edit_question_time_limit_field(self, time_limit : int , question : 'Question.Question') :
        try:
            question.time_limit = time_limit
            question.save()

            logging.debug("edited question time_limit field")

        except Exception as exception : 
            logging.error(exception)

    def __parameter_is__not_null(self, value : Any) -> bool :
        """ checks if the value is not null
        Parameters
        ----------
        value : Any
        Returns
        -------
        is_null : bool 
        """
        return value is not None 
    
    def add_question_to_session_participation_metadata(self, session_participation_metadata : 'Session.SessionParticipationMetadata', question : 'Question.Question'):
        try:
            session_participation_metadata.questions.add(question)
            session_participation_metadata.save()
        except Exception as exception :
            logging.error(exception)
    

   
    
        




        

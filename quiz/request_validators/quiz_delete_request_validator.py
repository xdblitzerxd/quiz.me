from django.http import QueryDict
from django.utils.datastructures import MultiValueDict
from .parent_quiz_request_validator import ParentQuizRequestValidator
from typing import Dict
import logging


class QuizDeleteRequestValidator(ParentQuizRequestValidator):

    def validate_delete_questions_from_quiz_request(self, request_data : QueryDict) -> Dict :
        """ validates delete_questions_from_quiz POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        

        try:
            questions_data = request_data["questions"]
            quiz_id = int(request_data["quiz_id"])  

            self._validate_integer(quiz_id)

            for question_data in questions_data : 
                question_id = int(question_data["question_id"])
                self._validate_integer(question_id)

            return {
                "quiz_id": quiz_id,
                "questions" : questions_data
            }

        except Exception as exception:
            logging.error(exception)
            return None

    def validate_delete_options_from_questions_in_quiz_request(self, request_data : QueryDict) -> Dict :
        """ validates delete_options_from_questions POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
       

        try:
            options_data = request_data["options"]
      
            for option_data in options_data :
                option_id = int(option_data["id"])
                self._validate_integer(option_id)
                
            return {
                "options" : options_data
            }

        except Exception as exception:
            logging.error(exception)
            return None

    def validate_delete_quiz_request(self, request_data : QueryDict) -> Dict :
        """ validates delete_quiz POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        quiz_id = int(request_data["quiz_id"])


        try :
            self._validate_integer(quiz_id)
            return {
                "quiz_id": quiz_id
            }

        except Exception as exception:
            logging.error(exception)
            return None


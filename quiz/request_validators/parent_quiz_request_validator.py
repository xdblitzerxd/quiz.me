from django.core.validators import validate_integer
from django.core.exceptions import ValidationError
import logging

class ParentQuizRequestValidator():
    def _validate_integer(self, value : int):
        """ validates integer value 

        Parameters
        ----------
        value : int 

        Returns
        -------
        """
        try : 
            validate_integer(value= value)
        except ValidationError as error : 
            logging.error(error)
            raise 
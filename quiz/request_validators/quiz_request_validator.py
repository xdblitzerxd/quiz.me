from django.http import QueryDict
from django.utils.datastructures import MultiValueDict
from quiz.forms.quiz_form import QuizForm
from quiz.forms.question_form import QuestionForm
from quiz.forms.option_form import OptionForm
from django.core.exceptions import ValidationError
from .parent_quiz_request_validator import ParentQuizRequestValidator
from typing import Dict, List 
import json
import logging


class QuizRequestValidator(ParentQuizRequestValidator):
    """
    Validates requests parameters data for quiz application POST requests 
    (use forms in quiz/forms)
    """

    def validate_add_options_to_questions_in_quiz_request(self, request_data : QueryDict) -> Dict :
        """ validates add_options_to_questions_in_quiz POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        quiz_id = int(request_data["quiz_id"])
        questions_data = request_data.get("questions")


        try :
            self._validate_integer(quiz_id)
            self.__validate_questions_data_with_id(questions_data = questions_data)

            logging.debug(msg="successfully validated add_options_to_questions_in_quiz create request")
            
            return {
                "quiz_id" : quiz_id,
                "questions" : questions_data
            }
        except Exception as exception :
            logging.error(exception)
            return None 

    def validate_add_questions_to_quiz_request(self, request_data : QueryDict) -> Dict : 
        """ validates add_questions_to_quiz POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        quiz_id = int(request_data["quiz_id"])
        questions_data = request_data.get("questions")
        

        try :
            self._validate_integer(quiz_id)
            self.__validate_questions_data_with_fields(
                questions_data = questions_data)

            logging.debug(msg="successfully validated add questions to quiz create request")

            return {
                "quiz_id" : request_data["quiz_id"],
                "questions" : questions_data
            }
        
        except Exception as exception :
            logging.error(exception)
            return None 




    def validate_create_quiz_request(self, request_data: QueryDict) -> Dict:
        """ validates create_quiz POST request data by instantiating corresponding forms with
        request_data object and returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        title = request_data["title"]
        description = request_data["description"]
        time_limited = bool(request_data["time_limited"])
        is_private = bool(request_data["is_private"])
        questions_data = request_data.get("questions")

        try :
            self.__validate_quiz(
                title= title, time_limited= time_limited, description= description, is_private= is_private)

            self.__validate_questions_data_with_fields(questions_data =questions_data)

            logging.debug(msg="successfully validated quiz create request")

            return {
                "title": title,
                "description": description,
                "is_private": is_private,
                "time_limited": time_limited,
                "questions": questions_data,
            }   

        except Exception as exception :
            logging.error(exception)
            return None 

       

    def __validate_quiz(self, title: str,
                        description: str, time_limited: bool, is_private: bool):
        """
        Validates data for quiz object using QuizFormInstance 

        Parameters
        ----------
        title : str
        description : str
        time_limited : bool 
        is_private : bool 

        Returns
        -------
        """
        quiz_form = QuizForm(data ={
            'title': title,
            'description': description,
            'time_limited': time_limited,
            'is_private': is_private
        })
        if not quiz_form.is_valid():
            raise ValidationError("quiz form is invalid")

        logging.debug(msg="validated quiz")

    def __validate_questions_data_with_fields(self, questions_data: List):
        """
        Validates data for list of question objects data with question fields 

        Parameters
        ----------
        questions : List

        Returns
        -------
        """
        for question_data in questions_data:

            options_data = question_data["options"]

            try :
                self.__validate_single_question_fields_data(question_data)
                self.__validate_options_data(options_data)
            except Exception as exception:
                logging.error(exception)
                raise 
    
    def __validate_questions_data_with_id(self, questions_data : List):
        """
        Validates data for list of question objects data with question ids  

        Parameters
        ----------
        questions : List

        Returns
        -------
        """
        for question_data in questions_data:
            options_data = question_data["options"]
            question_id = int(question_data["id"])

            try :
                self._validate_integer(question_id)
                self.__validate_options_data(options_data)
            except Exception as exception:
                logging.error(exception)
                raise 
                
        

    def __validate_single_question_fields_data(self, question_data : Dict):
        """
        Validates single question object fields data
        Parameters
        ----------
        question_data : Dict

        Returns
        -------
        """

        question_form = QuestionForm(
            data = question_data
        )

        if not question_form.is_valid():
            raise ValidationError("question form is invalid")

    def __validate_options_data(self, options_data: List):
        """
        Validates data for the list of option objects data for a given question 

        Parameters
        ----------
        questions : MultiValueDict 

        Returns
        -------
        """
        for option_data in options_data:

            option_form = OptionForm(data = option_data)

            if not option_form.is_valid():
                raise ValidationError("Option form is invalid")
    
   

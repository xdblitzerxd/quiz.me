from django.http import QueryDict
from typing import Dict, List
from django.core.exceptions import ValidationError
from .parent_quiz_request_validator import ParentQuizRequestValidator
from .serializers import *
import logging



class QuizEditRequestValidator(ParentQuizRequestValidator):

    def validate_edit_questions_in_quiz_request(self, request_data : QueryDict) -> Dict :
        """ validates edit_questions_in_quiz POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        quiz_id = int(request_data["quiz_id"])
        questions_data = request_data["questions"]

        try:
            self._validate_integer(quiz_id)
            self.__validate_edited_questions_data(questions_data)
            return {
                "quiz_id" : quiz_id,
                "questions" : questions_data
            }
        except Exception as exception: 
            logging.error(exception, exc_info= True)
            return None

    def validate_edit_quiz_title_request(self, request_data : QueryDict) -> Dict : 
        """ validates edit_quiz_title POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        title = request_data["title"]
        quiz_id = int(request_data["id"])
        

        try: 
            self._validate_integer(quiz_id)
            self.__validate_quiz_title_field(title = title)

            return {
                "id" : quiz_id,
                "title" : title   
            }

        except Exception as exception:
            logging.error(exception)
            return None

    def validate_edit_quiz_description_request(self, request_data : QueryDict) -> Dict : 
        """ validates edit_quiz_description POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """
        description = request_data["description"]
        quiz_id = int(request_data["id"])
        

        try:
            self._validate_integer(quiz_id)
            self.__validate_quiz_description_field(description= description)
            
            return {
                "id" : quiz_id,
                "description" : description
            }

        except Exception as exception:
            logging.error(exception)
            return None

    def validate_edit_quiz_privacy_request(self, request_data : QueryDict) -> Dict : 
        """ validates edit_quiz_privacy POST request data returns Dict of request data if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        validated_data : Dict
        """

        quiz_id = int(request_data["id"])
        is_private = bool(request_data["is_private"])

   

        try:
            self._validate_integer(quiz_id)
            self.__validate_quiz_privacy_field(is_private= is_private)
            
            return {
                "id" : quiz_id,
                "is_private" : is_private
            }

        except Exception as exception:
            logging.error(exception)
            return None
    
    def __validate_edited_questions_data(self, questions_data : List):
        for question_data in questions_data:
            options_data = question_data["options"]
            question_edited_fields_data = question_data["edited_fields"]

            self.__validate_single_question_edited_fields_data(question_edited_fields_data)
            self.__validate_edited_options_data(options_data)
    
    def __validate_edited_options_data(self, options_data : List):
        for option_data in options_data:

            option_edited_fields_data = option_data["edited_fields"]

            self.__validate_single_option_edited_fields_data(option_edited_fields_data)

    def __validate_single_question_edited_fields_data(self, question_data : Dict):
        text = question_data.get("text")
        time_limit = int(question_data.get("time_limit", "0"))

        question_edit_serializer = QuestionEditSerializer(
            data = {
                "text" : text,
                "time_limit" : time_limit
            }
        )

        if not question_edit_serializer.is_valid():
            raise ValidationError("question edited fields are invalid")
    
    def __validate_single_option_edited_fields_data(self, option_data : Dict):
        text = option_data.get("text")
        is_correct = bool(option_data.get("is_correct", "False"))

        option_edit_serializer = OptionEditSerializer(
            data= {
                "text" : text,
                "is_correct" : is_correct
            }
        )

        if not option_edit_serializer.is_valid():
            raise ValidationError("option edited fields are invalid")
            
    def __validate_quiz_title_field(self, title : str):
        title_serializer = QuizTitleEditRequestSerializer(
            data = {
                "title" : title
            })

        if not title_serializer.is_valid():
            raise ValidationError("Edit quiz title request data is invalid")
            
        logging.debug("successfully validated edit_quiz_title post request data")
    

    def __validate_quiz_description_field(self, description : str):
        description_serializer = QuizDescriptionEditRequestSerializer(
            data= {
                "description" : description
            })

        if not description_serializer.is_valid():
                raise ValidationError("Edit description post request data is invalid")
            
        logging.debug("successfully validated edit_quiz_description post request data")
    
    def __validate_quiz_privacy_field(self, is_private : bool):
        privacy_serializer = QuizPrivacyEditRequestSerializer(
            data = {
                "is_private" : is_private
            }
        )

        if not privacy_serializer.is_valid():
            raise ValidationError("Edit privacy post request data is invalid")
        
        logging.debug("successfully validated edit_quiz_privacy post request data")
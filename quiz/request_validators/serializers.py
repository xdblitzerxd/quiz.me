from rest_framework.serializers import Serializer, CharField, BooleanField, IntegerField

class QuizTitleEditRequestSerializer(Serializer):
    title = CharField(max_length = 20)

class QuizDescriptionEditRequestSerializer(Serializer):
    description = CharField(max_length = 300)

class QuizPrivacyEditRequestSerializer(Serializer):
    is_private = BooleanField()


class QuestionEditSerializer(Serializer):
    text = CharField(max_length = 80, allow_null = True)
    time_limit = IntegerField(allow_null = True)


class OptionEditSerializer(Serializer):
    text = CharField(max_length = 80, allow_null = True)
    is_correct = BooleanField(allow_null= True)


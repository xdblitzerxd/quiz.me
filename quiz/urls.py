from django.contrib import admin
from django.urls import path
from .views import QuizMainPageView, QuizCreatePageView, QuizPageView, QuizSettingsPageView, QuizSessionPageView
app_name = 'quiz'

urlpatterns = [
    path('', QuizMainPageView.as_view(), name = "main_page"),
    path('create/', QuizCreatePageView.as_view(), name = "create_page"),
    path('<int:id>/', QuizPageView.as_view(), name = "quiz_page"),
    path('<int:id>/settings/', QuizSettingsPageView.as_view(), name= "quiz_settings_page"),
    path('<int:quiz_id>/session/<int:session_id>/', QuizSessionPageView.as_view(), name= "quiz_session_page")
]

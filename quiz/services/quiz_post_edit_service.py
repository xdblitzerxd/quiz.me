from django.http import HttpRequest
from quiz.models.Quiz import Quiz
from quiz.models.Question import Question
from quiz.models.Option import Option
from typing import Dict, List
import logging
from quiz.request_validators.quiz_edit_request_validator import QuizEditRequestValidator



class QuizPostEditService():
    """
    Class that takes quiz application post edit
    requests data from view functions, validates if necessary, updates
    corresponding data in the database and then returns
    corresponding context array
    """


    def __init__(self) -> None:
        self.validator = QuizEditRequestValidator()

    def process_edit_questions_in_quiz_request(self, request_data : Dict): 
        """ processes edit_questions_in_quiz post request

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """

        

        validated_data = self.validator.validate_edit_questions_in_quiz_request(request_data)

        self.__edit_questions(questions_data= validated_data["questions"])



        
        

            
    def __edit_questions(self, questions_data : List):
        """
        Takes questions data , and calles internal edition methods for question object
        itself and corresponding options to each one 

        Parameters
        ----------
        questions_data : List

        Returns
        ------- 
        """
        question_manager = Question.manager 

       
        for question_data in questions_data:
            question = question_manager.get_question_by_id(question_data["id"])

            self.__edit_fields_for_question(
                edited_fields_data = question_data["edited_fields"],
                question= question
            ) 

            self.__edit_options(options_data = question_data["options"])
       

           

            
    
    def __edit_fields_for_question(self, edited_fields_data : Dict, question : Question):
        """
        Takes edit_fields_data and calls corresponding manager method

        Parameters
        ----------
        edited_fields_data : Dict 
        question : Question 

        Returns
        ------- 
        """
        question_manager = Question.manager

        question_manager.edit_fields_for_question(question = question, **edited_fields_data)

    def __edit_options(self, options_data : List): 
        """
        Takes options_data and calls corresponding internal method for each option edition

        Parameters
        ----------
        options_data : List

        Returns
        ------- 
        """

        for option_data in options_data : 
            self.__edit_fields_for_option(
                edited_fields_data= option_data["edited_fields"], option_id = option_data["id"])
            

    def __edit_fields_for_option(self, edited_fields_data : Dict,  option_id : int):
        """
        Takes edited_fields_data for each question and calls corresponding manager methods

        Parameters
        ----------
        edited_fields_data : Dict 
        option_id : int 

        Returns
        ------- 
        """
        option_manager = Option.manager

        option = option_manager.get_option_by_id(id = option_id) 
        
        option_manager.edit_fields_for_option(option, **edited_fields_data)


        



    def process_edit_quiz_title_request(self, request_data : Dict):
        """ processes edit_quiz_title post request 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """

        quiz_manager = Quiz.manager 
        
        validated_data = self.validator.validate_edit_quiz_title_request(request_data)

        quiz = quiz_manager.get_quiz_by_id(id = validated_data["id"])
        
        quiz_manager.edit_quiz_title_field(quiz = quiz, title = validated_data["title"])
     
    
    def process_edit_quiz_description_request(self, request_data : Dict): 
        """ processes edit_quiz_description post request 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """

        quiz_manager = Quiz.manager 
        
        validated_data = self.validator.validate_edit_quiz_description_request(request_data)

        quiz = quiz_manager.get_quiz_by_id(id = validated_data["id"])
        
        quiz_manager.edit_quiz_description_field(quiz = quiz, description = validated_data["description"])


    def process_edit_quiz_privacy_request(self, request_data : Dict): 
        """ processes edit_quiz_privacy post request 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """
        
        quiz_manager = Quiz.manager 

        validated_data = self.validator.validate_edit_quiz_privacy_request(request_data)

        quiz = quiz_manager.get_quiz_by_id(id = validated_data["id"])
        
        quiz_manager.edit_quiz_privacy_field(quiz = quiz, is_private = validated_data["is_private"])

        

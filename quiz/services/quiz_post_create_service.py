from django.http import HttpRequest
from quiz.models.Quiz import Quiz
from quiz.models.Question import Question
from typing import Dict, List
from quiz.request_validators.quiz_request_validator import QuizRequestValidator
from django.contrib.auth.models import User
import logging



class QuizPostCreateService():
    """
    Class that takes quiz application post 
    request data from view functions, validates if necessary, updates
    corresponding data in the database and then returns
    corresponding context array
    """


    def __init__(self) -> None:
        self.validator = QuizRequestValidator()

    def process_add_questions_to_quiz_request(self, request_data : Dict): 
        """ processes add_questions_to_quiz post request

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """

        quiz_manager = Quiz.manager

        validated_data = self.validator.validate_add_questions_to_quiz_request(request_data = request_data)
        quiz_id = validated_data["quiz_id"]
        quiz = quiz_manager.get_quiz_by_id(id = quiz_id)
    
        self.__create_questions_for_quiz(questions_data= validated_data["questions"], quiz = quiz)
        


       
        
    

    def process_add_options_to_questions_in_quiz_request(self, request_data : Dict):
        """ processes add_options_toquestions_in_quiz post request 

        Parameters
        ----------
        request : Dict

        Returns
        -------
        """
        validated_data = self.validator.validate_add_options_to_questions_in_quiz_request(request_data = request_data)

        self.__add_options_to_questions_in_quiz(validated_data["questions"])


        
      

       
    

    def __add_options_to_questions_in_quiz(self, questions_data : List):
        """
        Takes add options to questions data , creates option objects using OptionManager

        Parameters
        ----------
        questions_data : List

        Returns
        ------- 
        """

        question_manager = Question.manager

        for question_data in questions_data:
            question = question_manager.get_question_by_id(question_data["id"])  
            self.__create_options_for_question(options_data= question_data["options"], question= question)

     
                
    def process_create_quiz_request(self, request_data: Dict, request : HttpRequest):
        """
        Takes create quiz POST request parameters and creates quiz object (
            validates form data and creates quiz object in corresponding 
            manager
        )

        Parameters
        ----------
        request : HttpRequest
        request_data : Dict

        Returns
        ------- 
        """

        validated_data = self.validator.validate_create_quiz_request(request_data)

        self.__create_quiz(
            user=request.user, quiz_data=validated_data)
        

           

    def __create_quiz(self, user: User, quiz_data: Dict):
        """
        Takes create quiz_data, creates quiz_object, 
        calls questions instantiation methods

        Parameters
        ----------
        user : User 
        quiz_data : Dict 

        Returns
        ------- 
        """
        quiz_manager = Quiz.manager

        quiz = quiz_manager.create_quiz(
            user=user,
            time_limited= quiz_data["time_limited"],
            title=quiz_data["title"],
            description=quiz_data["description"],
            is_private=quiz_data["is_private"]
        )

        self.__create_questions_for_quiz(
            questions_data=quiz_data["questions"],
            quiz=quiz
        )

    
            

    def __create_questions_for_quiz(self, questions_data: List, quiz: Quiz):
        """
        Takes create questions data , creates questions objects, 
        calls options instantiation method for each question object

        Parameters
        ----------
        questions_data : List
        quiz : Quiz  

        Returns
        -------
        """
        question_manager = Question.manager

        for question_data in questions_data:

            question = question_manager.create_question_for_quiz(
                quiz=quiz,
                text=question_data["text"],
                time_limit=question_data["time_limit"],
                type=question_data["type"]
            )

            self.__create_options_for_question(
                options_data=question_data["options"],
                question=question
            )

           
                

  

    def __create_options_for_question(self, options_data: List, question: Question) -> bool:
        """
        Takes create options data , creates options objects

        Parameters
        ----------
        options_data : List
        question : Question 

        Returns
        ------- 
        """
        question_manager = Question.manager

        for option_data in options_data:
            question_manager.create_option_for_question(
                question=question,
                is_correct=bool(option_data["is_correct"]),
                field_type=option_data["field_type"],
                text=option_data["text"]
            )

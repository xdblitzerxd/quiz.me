from django.http import HttpRequest
from quiz.models.Quiz import Quiz
from typing import Dict
import logging


class QuizGetService():
    """
    Class that takes quiz application get  
    request data from view functions, validates if necessary, fetches 
    corresponding data from the database and then returns
    corresponding context array
    """

    def get_quizes_page_context(self, request_data : Dict) -> Dict:
        """ returns context for quizes page from managers 

        Parameters
        ----------
        request_data : Dict 

        Returns
        -------
        context : Dict [
            quizes : QuerySet<Quiz>
        ]
        """
        quiz_manager = Quiz.manager
        quizes = quiz_manager.get_all_public_quizes()

        context = {
            'quizes': quizes
        }

        logging.debug(
            msg=f"Returned quizes page get request context : {context}")

        return context

    def get_quiz_page_context(self, request_data: Dict, quiz_id: int) -> Dict:
        """ returns context for quiz page from managers based on quiz id 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        context : Dict [
            quiz : Quiz
        ]
        """
        quiz_manager = Quiz.manager

        quiz = quiz_manager.get_quiz_by_id(id=quiz_id)

        context = {
            'quiz': quiz
        }

        logging.debug(
            msg=f"Returned quiz page get request context : {context}")

        return context

    def get_quiz_settings_page_context(self, request_data : Dict, quiz_id: int) -> Dict:
        """ returns context for quiz settings page from managers based on quiz id 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        context : Dict [
            quiz : Quiz
        ]
        """

        quiz_manager = Quiz.manager

        quiz = quiz_manager.get_quiz_by_id(id=quiz_id)

        context = {
            'quiz': quiz
        }

        logging.debug(
            msg=f"Returned quiz settings page get request context : {context}")

        return context

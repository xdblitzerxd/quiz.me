from django.http import HttpRequest
from quiz.models.Quiz import Quiz
from quiz.models.Question import Question
from quiz.models.Option import Option
from typing import Dict, List
import logging
from quiz.request_validators.quiz_delete_request_validator import QuizDeleteRequestValidator


class QuizPostDeleteService():


    def __init__(self) -> None:
        self.validator = QuizDeleteRequestValidator()

    def process_delete_questions_from_quiz_request(self, request_data : Dict): 
        """ processes delete_questions_from_quiz post request 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """
        validated_data = self.validator.validate_delete_questions_from_quiz_request(request_data)

        self.__delete_questions(questions_data = validated_data["questions"])

    
    

    def __delete_questions(self, questions_data : List):
        """ deletes questions from quiz using 
        questions_data and by calling manager methods

        Parameters
        ----------
        questions_data : List

        Returns
        -------
        """
        question_manager = Question.manager
       

        for question_data in questions_data:
            question_id = question_data["question_id"]
            question = question_manager.get_question_by_id(id = question_id)

            question_manager.delete_question(question_id= question.pk)
        



    def process_delete_options_from_questions_in_quiz_request(self, request_data : Dict):
        """ processes delete_options_from_questions_in_quiz post request 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """
        validated_data = self.validator.validate_delete_options_from_questions_in_quiz_request(request_data)

        self.__delete_options_from_questions_in_quiz(options_data= validated_data["options"])
            


    
    def __delete_options_from_questions_in_quiz(self, options_data : List):
        """
        Takes delete options to questions data , deletes option objects using OptionManager

        Parameters
        ----------
        options_data : List 

        Returns
        ------- 
        """
        for option_data in options_data:
            self.__delete_single_option(option_data)
            
       
    

    def __delete_single_option(self, option_data : Dict):
        """
        Deletes single option by calling corresponding option_manager methods

        Parameters
        ----------
        option_data : Dict

        Returns
        -------
        """
        option_manager = Option.manager
        option = option_manager.get_option_by_id(id = option_data["id"])
        option_manager.delete_option(option)

    def process_delete_quiz_request(self, request_data : Dict):
        """ processes delete__quiz post request 

        Parameters
        ----------
        request_data : Dict

        Returns
        -------
        """
        validated_data = self.validator.validate_delete_quiz_request(request_data)

        quiz_manager = Quiz.manager
        
        quiz_id = validated_data["quiz_id"]

        quiz = quiz_manager.get_quiz_by_id(id = quiz_id)

        quiz_manager.delete_quiz(quiz)

            
        
        
from django.db import models
from django.contrib.auth.models import User
from datetime import date
import account.models as account_models
import logging


class UserProfileManager(models.Manager):

    def get_profile_for_user(self, user: User) -> 'account_models.UserProfile':
        """  
        Returns user profile model instance by user object
        Parameters
        ----------
        user : User 

        Returns
        --------
        profile : UserProfile 
        """
        return self.get_queryset().filter(user=user)

    def create_profile_for_user(self, user: User, description: str,  profile_image_url: str, birth_date: date) -> bool:
        """  
        create UserProfile object for user and returns result flag 
        Parameters
        ----------
        user : User,
        description : str,
        profile_image_url : str,
        birth_date : date 
        Returns
        --------
        profile : UserProfile 
        """
        try:
            profile = self.model(
                user=user,
                description=description,
                profile_image_url=profile_image_url,
                birth_date=birth_date
            )

            profile.save()
            return True

        except Exception as exception:
            logging.error(msg=f"{exception}")

            return False

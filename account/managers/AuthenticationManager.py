
from django.contrib.auth.models import UserManager
from django.contrib.auth import login, authenticate
from django.http import HttpRequest

import logging


class AuthenticationManager(UserManager):

    def authenticate_user(self, request: HttpRequest, username: str, password: str) -> bool:
        """ authenticates user and returns result flag 

        Parameters
        ----------
        username : string 
        password : string 

        Returns
        -------
        authenticated_user : Bool 
        """
        try:
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request=request, user=user)
                return True

        except Exception as exception:
            logging.error(msg=f"{exception}")

        return False

    def register_user(self, username: str, password: str, email: str) -> bool:
        """ registers user and returns result flag 

        Parameters
        ----------
        username: string 
        email : string
        password: string


        Returns
        -------
        registered_user: Bool
        """
        try:
            user = super().create_user(username=username, password=password, email=email)

            user.save()
            return True

        except Exception as exception:
            logging.error(msg=f"{exception}")

        return False
    
    def register_user(self, username : str, password : str, email : str, id : int):
        try:
            user = super().create_user(username=username, password=password, email=email, id = id)
            user.save()

            return user

        except Exception as exception:
            logging.error(msg=f"{exception}")
            return None
    
    def get_user_by_id(self, id : int):
        return super().get_queryset().get(id = id)
    
    
from django.http import QueryDict
from account.forms.register_form import RegisterForm
from account.forms.login_form import LoginForm
import logging


class AuthenticationRequestValidator():

    """
    Validates requests parameters data for account application POST requests 
    (use forms in account/forms)
    """

    def validate_register_request(self, request_data: QueryDict) -> bool:
        """ validates register_user POST request data by instantiating 
        corresponding forms with request_data object and returns True 
        if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        is_valid : bool 
        """
        # email = request_data['email']
        # password1 = request_data['password1']
        # password2 = request_data['password2']
        # username = request_data['username']

        form = RegisterForm(request_data)

        if form.is_valid():
            logging.debug(msg="register form is valid")
            return True
        else:
            logging.error(msg=f"register form is not valid")
            return False

    def validate_login_request(self, request_data: QueryDict) -> bool:
        """ validates login_user POST request data by instantiating 
        corresponding forms with request_data object and returns True 
        if the POST request data is valid. 

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        is_valid : bool 
        """
        # username = request_data['username']
        # password = request_data['password']

        form = LoginForm(data=request_data)

        if form.is_valid():
            logging.debug(msg="login form is valid")
            return True
        else:
            logging.error(
                msg=f"login form is not valid, is bound : {form.is_bound}")
            return False

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpRequest
from .services.authentication_post_service import AuthenticationPostService

# Create your views here.


@login_required(login_url='/', redirect_field_name=None)
def account_page(request: HttpRequest):
    return render(request, template_name="account.html")


@user_passes_test(test_func=lambda user: not user.is_authenticated, login_url='/', redirect_field_name=None)
def register_page(request: HttpRequest):
    if request.method == "POST":
        authentication_post_service = AuthenticationPostService()
        authentication_post_service.process_register_user_request(request)

        return redirect("home:main")

    return render(request, template_name="register.html")


@user_passes_test(test_func=lambda user: not user.is_authenticated, login_url='/', redirect_field_name=None)
def login_page(request: HttpRequest):
    if request.method == "POST":
        authentication_post_service = AuthenticationPostService()
        authentication_post_service.process_authenticate_user_request(request)

        return redirect("home:main")

    return render(request, template_name="login.html")

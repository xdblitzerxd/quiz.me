from django.http import HttpRequest
from ..request_validators.authentication_request_validator import AuthenticationRequestValidator
from django.contrib.auth.models import User
import logging


class AuthenticationPostService():
    """
    Class that takes account application post 
    request data from view functions, validates if necessary, updates
    corresponding data in the database and then returns
    corresponding context array
    """

    def process_authenticate_user_request(self, request: HttpRequest) -> bool:
        """
        Takes login POST request parameters and authenticates user (
            validates form data and authenticates user in corresponding 
            manager
        )

        Parameters
        ----------
        request : HttpRequest

        Returns
        -------
        authenticated_user : bool 
        """
        validator = AuthenticationRequestValidator()

        manager = User.objects

        request_data = request.POST

        validated_data = validator.validate_login_request(request_data)

        if validated_data:
            authenticated_user = manager.authenticate_user(
                username=request_data["username"],
                password=request_data["password"],
                request=request
            )
            logging.debug(
                msg=f"Authenticated user flag : {authenticated_user}")

            return authenticated_user

        logging.error(
            msg="Failed to vaildate authenticate user post request data")

        return False

    def process_register_user_request(self, request: HttpRequest) -> bool:
        """
        Takes register POST request parameters and register user (
            validates form data and registers user in corresponding 
            manager
        )

        Parameters
        ----------
        request : HttpRequest

        Returns
        -------
        registered_user : bool 
        """
        validator = AuthenticationRequestValidator()

        manager = User.objects

        request_data = request.POST

        validated_data = validator.validate_register_request(request_data)

        if validated_data:
            registered_user = manager.register_user(
                username=request_data['username'],
                email=request_data['email'],
                password=request_data['password1']
            )

            logging.debug(msg=f"registered user flag : {registered_user}")

            return registered_user

        logging.error(msg="Failed to vaildate register user post request data")

        return False

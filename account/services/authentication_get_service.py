from django.http import HttpRequest
from typing import Dict
from django.contrib.auth.models import User


class AuthenticationGetService():
    """
    Class that takes account application get  
    request data from view functions, validates if necessary, fetches 
    corresponding data from the database and then returns
    corresponding context array
    """

    def get_context_for_profile_page(request: HttpRequest) -> Dict:
        """ returns context for profile page based on specific user data 
        from managers 

        Parameters
        ----------
        request : HttpRequest

        Returns
        -------
        context : Dict [
            userProfile : UserProfile
        ]
        """
        pass

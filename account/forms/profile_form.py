from django.forms import ModelForm
from django import forms
from account.models import UserProfile
from typing import Dict, Any
import logging


class ProfileForm(ModelForm):
    """
    Form that takes following UserProfile model parameters and validates them

    description
    birth_date 
    profile_image_url 
    """
    class Meta:
        model = UserProfile
        fields = ['description', 'birth_date', 'profile_image_url']

    def full_clean(self) -> None:
        try:
            super().full_clean()

        except forms.ValidationError as error:
            logging.error(f"profile form validation error: {error}")

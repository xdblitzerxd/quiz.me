from django.contrib.auth.forms import UserCreationForm
from django import forms
from account.models import UserProfile
from typing import Dict, Any
import logging


class RegisterForm(UserCreationForm):
    """
    Implements register post requests parameters
    validation functionality
    """
    # email = forms.EmailField(label='', widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Почта'}), min_length=10, max_length=70)
    # password1 = forms.CharField(label='', widget=forms.PasswordInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Введите пароль'}), min_length=6, max_length=100)
    # password2 = forms.CharField(label='', widget=forms.PasswordInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Введите пароль повторно'}), min_length=6, max_length=100)
    # username = forms.CharField(label='', widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Имя'}), min_length=6, max_length=60)

    # class Meta:
    #     model = UserProfile
    #     fields = ['birth_date']

    def full_clean(self) -> None:
        try:
            super().full_clean()

        except forms.ValidationError as error:
            logging.error(f"register form validation error: {error}")

    # def clean(self) -> Dict[str, Any]:

        # cleaned_data = super().clean()

        # logging.debug(f"register validation errors : {self.errors}")

        # return cleaned_data

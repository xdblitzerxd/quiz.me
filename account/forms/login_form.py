from django.contrib.auth.forms import AuthenticationForm
from account.models import UserProfile
from django import forms
from typing import Dict, Any
import logging


class LoginForm(AuthenticationForm):
    """
    Implements login post requests parameters
    validation functionality
    """
    # username = forms.CharField(label='', widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Почта'}), min_length=10, max_length=70)
    # password = forms.CharField(label='', widget=forms.PasswordInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Введите пароль'}), min_length=6, max_length=100)

    # def confirm_login_allowed(self, user):
    #     pass

    def full_clean(self) -> None:
        try:
            super().full_clean()

        except forms.ValidationError as error:
            logging.error(f"login form validation error: {error}")

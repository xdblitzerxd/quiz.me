from django.db import models
from .managers.AuthenticationManager import AuthenticationManager
from .managers.UserProfileManager import UserProfileManager
# from django.contrib.auth import get_user_model


class UserProfile(models.Model):
    user = models.OneToOneField(
        'auth.User', on_delete=models.CASCADE, primary_key=True)
    description = models.CharField(max_length=100)
    birth_date = models.DateField()
    profile_image_url = models.CharField(max_length=150)
    profile_manager = UserProfileManager()

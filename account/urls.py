from django.contrib import admin
from django.urls import path
from .views import account_page, register_page, login_page

app_name = 'account'

urlpatterns = [
    path('', account_page, name='profile'),
    path('register/', register_page, name='registration'),
    path('login/', login_page, name='login')
]

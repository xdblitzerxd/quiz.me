from django.apps import AppConfig


class AccountConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'account'

    def ready(self) -> None:
        from django.contrib.auth.models import User
        from .managers.AuthenticationManager import AuthenticationManager

        manager = AuthenticationManager()
        manager.model = User
        User.objects = manager

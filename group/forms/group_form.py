from django.forms import ModelForm
from group.models import Group
import logging


class GroupForm(ModelForm):
    """
    Form that takes following group model parameters and validates them

    title
    description 
    is_private 
    """
    class Meta:
        model = Group
        fields = ['title', 'description', 'is_private']

    def clean(self):
        cleaned_data = super().clean()

        if self.errors:
            logging.error(f"{self.errors}")

        return cleaned_data

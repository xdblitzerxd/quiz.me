from django.db import models
from django.contrib.auth.models import User
from django.db.models import IntegerField
import group.models as group_models


class GroupManager(models.Manager):

    def get_all_public_groups(self) -> models.QuerySet['group_models.Group']:
        """ returns all public groups 

        Parameters
        ----------
        None 

        Returns
        -------
        all_public_groups : QuerySet<Group>
        """
        all_public_groups = self.get_queryset(is_private=False)
        return all_public_groups

    def create_group(self, is_private: bool, title: str, description: str, author: User) -> bool:
        """ creates a group

        Parameters
        ----------
        title : string 
        description: string 
        author: User
        is_private: bool 

        Returns
        -------
        result_flag : bool 
        """
        try:
            group = self.model(
                title=title,
                is_private=is_private,
                description=description
            )

            group.members.add(author, through_defaults={'is_author': True})

            group.save()

            return True

        except:
            return False

    def delete_group(self, group_id: IntegerField) -> bool:
        """ deletes a group 

        Parameters
        ----------
        group_id : IntegerField 

        Returns
        -------
        result_flag : bool 
        """
        try:
            group = self.get_queryset().get(id=group_id)
            group.delete()
            return True
        except:
            return False

    def get_all_groups_participated_by_user(self, user: User) -> models.QuerySet['group_models.Group']:
        """ returns all groups participated  by a given user 

        Parameters
        ----------
        user : User 

        Returns
        -------
        all_groups_participated_by_user : QuerySet<Group>
        """
        all_groups_participated_by_user = self.get_queryset().filter(members=user)
        return all_groups_participated_by_user

    def add_member_to_group(self, member: User, group: 'group_models.Group') -> bool:
        """ adds a member to a group 

        Parameters
        ----------
        member: User 
        group: Group 

        Returns
        -------
        result_flag: bool 
        """
        try:
            group.members.add(member, through_defaults={'is_author': False})
            group.save()

            return True
        except:

            return False

    def delete_member_from_group(self, member: User, group: 'group_models.Group') -> bool:
        """ deletes a member from a group 

        Parameters
        ----------
        member: User 
        group: Group 

        Returns
        -------
        result_flag: bool 
        """
        try:
            group.members.delete(member)
            group.save()

            return True

        except:
            return False

    def toggle_group_privacy(self, group: 'group_models.Group', is_private: bool) -> bool:
        """ toggles group privacy

        Parameters
        ----------
        group: Group 
        is_private : bool

        Returns
        -------
        result_flag: bool 
        """
        try:
            if is_private:
                group.is_private = True
            else:
                group.is_private = False

        except:
            return False

    def get_group_by_id(self, id: IntegerField) -> 'group_models.Group':
        """ returns group object by id 

        Parameters
        ----------
        id : IntegerField 

        Returns
        -------
        group : Group 
        """
        return self.get_queryset().get(id=id)

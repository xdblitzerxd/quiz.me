from django.http import QueryDict
from forms.group_form import GroupForm


class GroupRequestValidator():
    """
    Validates requests parameters data for group application POST requests 
    (use forms in group/forms)
    """

    def validate_create_group_request(request_data: QueryDict) -> bool:
        """ validates create_group POST request data by instantiating 
        corresponding forms with request_data object and returns True 
        if the POST request data is valid.

        Parameters
        ----------
        request_data : QueryDict 

        Returns
        -------
        is_valid : bool 
        """
        title = request_data['title']
        description = request_data['description']
        is_private = request_data['is_private']

        form = GroupForm(initial={
            'title': title,
            'description': description,
            'is_private': is_private
        })

        if form.is_valid():
            return True
        else:
            return False

    def validate_delete_group_request(request_data: QueryDict) -> bool:
        pass

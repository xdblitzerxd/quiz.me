from django.db import models
from .managers.GroupManager import GroupManager


class Group(models.Model):
    manager = GroupManager()
    title = models.CharField(max_length=40)
    description = models.CharField(max_length=200)
    members = models.ManyToManyField(through='GroupToUser', to='auth.User')
    quizes = models.ManyToManyField(through='GroupToQuiz', to='quiz.Quiz')
    is_private = models.BooleanField()


class GroupToUser(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    group = models.ForeignKey('Group', on_delete=models.CASCADE)
    is_author = models.BooleanField(default=False)


class GroupToQuiz(models.Model):
    quiz = models.ForeignKey('quiz.Quiz', on_delete=models.CASCADE)
    group = models.ForeignKey('Group', on_delete=models.CASCADE)
    created_in_group = models.BooleanField()
    shared_in_group = models.BooleanField()

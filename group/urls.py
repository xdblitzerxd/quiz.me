from django.contrib import admin
from django.urls import path
from .views import group_create_page, group_settings_page, group_page, main_page

app_name = 'group'

urlpatterns = [
    path('', main_page),
    path('create/', group_create_page),
    path('<int:id>/settings', group_settings_page),
    path('<int:id>', group_page)
]

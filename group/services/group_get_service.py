from django.http import HttpRequest
from typing import Dict


class GroupGetService():
    """
    Class that takes group application get  
    request data from view functions, validates if necessary, fetches 
    corresponding data from the database and then returns
    corresponding context array
    """

    def get_context_for_groups_page(request: HttpRequest) -> Dict:
        """ returns context for groups page from managers 

        Parameters
        ----------
        request : HttpRequest

        Returns
        -------
        context : Dict [
            groups : QuerySet<Group>
        ]
        """
        pass

    def get_context_for_group_page(request: HttpRequest, group_id: int) -> Dict:
        """ returns context for group page from managers based on group id 

        Parameters
        ----------
        request : HttpRequest

        Returns
        -------
        context : Dict [
            group : Group 
        ]
        Dict
        """
        pass

    def get_context_for_group_settings_page(request: HttpRequest, group_id: int) -> Dict:
        """ returns context for group settings page from managers based on group id 

        Parameters
        ----------
        request : HttpRequest

        Returns
        -------
        context : Dict [
            group : Group 
        ]
        """
        pass

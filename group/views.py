from django.shortcuts import render

# Create your views here.


def group_create_page(request):
    return render(request, template_name='group_create.html')


def main_page(request):
    return render(request, template_name='group_main.html')


def group_settings_page(request):
    return render(request, template_name='group_settings.html')


def group_page(request):
    return render('group.html')

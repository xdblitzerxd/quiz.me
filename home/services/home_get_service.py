from django.http import HttpRequest
from quiz.models.Quiz import Quiz
from typing import Dict
import logging


class HomeGetService():
    """
    Class that takes home application get  
    request data from view functions, validates if necessary, fetches 
    corresponding data from the database and then returns
    corresponding context array
    """

    def get_home_page_context(self, request: HttpRequest) -> Dict:
        """ returns context for main page from managers 

        Parameters
        ----------
        request : HttpRequest

        Returns
        -------
        context : Dict [
            quizes : QuerySet<Quiz>
        ]
        """
        quiz_manager = Quiz.manager
        quizes = quiz_manager.get_all_public_quizes()

        context = {
            'quizes': quizes
        }

        logging.debug(
            msg=f"Returned main page get request context : {context}")

        return context

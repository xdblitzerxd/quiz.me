from django.shortcuts import render
from django.http import HttpRequest
from .services.home_get_service import HomeGetService

# Create your views here.


def main_page(request: HttpRequest):
    context = {}
    if request.method == "GET":
        quiz_get_service = HomeGetService()
        context = quiz_get_service.get_home_page_context(request)

    return render(request, template_name='home.html', context=context)

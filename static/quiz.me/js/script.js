const addButton = document.getElementById("add-question-group");
const scene = document.getElementById("scene");

let groupCount = 0;

addButton.addEventListener("click", () => {
  groupCount++;

  const group = document.querySelector(".question_group").cloneNode(true);

  if (groupCount % 2 === 0) {
    group.style.marginRight = "0";
  }

  scene.appendChild(group);

  // Check if there are more than 2 groups in a row
  const groupsInRow = scene.querySelectorAll('.question_group:nth-child(2n)');
  if (groupsInRow.length >= 2) {
    const lastGroupInRow = groupsInRow[groupsInRow.length - 1];
    lastGroupInRow.style.marginBottom = "0";
  }
});

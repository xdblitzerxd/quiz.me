#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
import logging


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'quiz_me.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(filename)s:%(lineno)d:%(message)s',level = logging.DEBUG)

    # error_handler = logging.StreamHandler()
    # error_handler.setLevel(level = logging.ERROR)
    # root_logger.addHandler(error_handler)

    # debug_handler = logging.StreamHandler()
    # # debug_handler.stream = sys.stdout

    # debug_handler.setLevel(level = logging.DEBUG)
    # debug_handler.stream = sys.stdout
    # root_logger.addHandler(debug_handler)

    main()
